var divs = document.getElementsByClassName('rollup');
var screenXY = [];
var pageBreaks =[];
var yOffset = 0;
document.getElementsByTagName("");
window.onload = function () {
    console.log('made it');
    setDivSize();
    window.onresize = function (){
        setDivSize();
    };
    window.onscroll = function (){
        yOffset= window.pageYOffset;
    enable_arrows();
    };

    enable_arrows();
};

document.onreadystatechange = function(){
    setDivSize();
    /*if (document.readyState === 'interactive'){
        //enable arrows asap
        setDivSize();
    }*/
};
function get_screensize() {

    let screenX = window.innerWidth;
    let screenY = window.innerHeight;
    return [screenX, screenY];
}

function setDivSize() {
    screenXY = get_screensize();
    for(let ele of divs){
        ele.setAttribute('width', screenXY[0]);
        ele.setAttribute('height', screenXY[1])
    }
    pageBreaks = setPageBreaks(screenXY, divs);
}

function setPageBreaks(screenXY, divs) {
    let count = 0;
    console.log(divs.length);
    for (let i of divs){
        console.log(count);
        pageBreaks[count] = (count * screenXY[1]) + 1;
        count++;
    }
    let header= document.getElementById('section_0');
     pageBreaks[1]=header.offsetHeight;
    enable_arrows();
    return pageBreaks;
    /*console.log(pageBreaks);*/
}


function go_fullscreen() {
    let body = document.getElementsByTagName('body')[0];
    body.requestFullscreen();
}

function enable_arrows() {
    let rollup =document.getElementsByClassName('scroll_arrow');
    for (let i of rollup) {
        i.onclick = function () {
            let pagetop = window.pageYOffset +1;
            console.log("pagetop: " + pagetop);
            let index= pageBreaks.findIndex(element=> element > pagetop );

            nextPage(index);
        }
    }
}

function nextPage(index) {
    //let nextdiv = document.getElementById('section_2');

    console.log(index);
    window.scrollTo({
        left: 0,
        top: pageBreaks[index],
        behavior: "smooth"
    });
    //nextdiv.scrollIntoView();
}